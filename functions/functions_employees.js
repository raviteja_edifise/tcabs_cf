// const admin = require('firebase-admin');

var is = require('is_js');
const currentPath = 'employees/'
// var db = require('./db');
// var app = db;

function addUser(req,res, adminInstance){

  const adminIdToken = req.body.idToken;
  const userData = req.body.userData;

  let userId = null, userType = null;

  if(is.not.existy(adminIdToken)) {throw new Error('missing data idToken'); response.status(500);};
  if(is.not.existy(userData)) {throw new Error('Sorry request failed'); response.status(500);};

  return adminInstance.auth().verifyIdToken(adminIdToken)
  .then(function(decodedToken) {
    var uid = decodedToken.uid;
    console.log(' Decoded token '+uid);
    return adminInstance.auth().createUser({
        uid : userData.uid,
        email: userData.email,
        emailVerified: false,
        phoneNumber: userData.contact,
        password: "secretPassword",
        displayName: userData.firstname+' '+userData.lastname,
        disabled: false,
        customClaims: { usertype : userData.usertype}
      });

  })
  .then(function(results){
    console.log("User Created ", userData);
    let updates = {};
    userData.status = "enabled";
    userData.contact = userData.phoneNumber;
    delete userData.phoneNumber;

    //RT DB
    // updates['employees/'+userData.uid] = userData;
    // updates[userData.usertype+'/'+userData.uid] = { time : (new Date).getTime()}
    // return adminInstance.database().ref().update(updates);

    //FS DB
    userId = userData.uid;
    userType = userData.usertype;

    var batch = adminInstance.firestore().batch();
    var nycRef = adminInstance.firestore().collection('employees').doc(''+userData.uid);
    batch.set(nycRef, userData);
    var nycRef2 = adminInstance.firestore().collection(''+userData.usertype).doc(''+userData.uid);
    batch.set(nycRef2, { time : (new Date).getTime()});
    return batch.commit();

  })
  .then(function(response){
    return adminInstance.auth().setCustomUserClaims(userId, {
      usertype : userType
    })
  })
  .then(function(results){
    res.status(200).send({ "response" : results, "error" : false });
  })
  .catch(function(error) {
    console.log('Error ', error);
    res.status(500);
  });
}



function importUsers(req,res, adminInstance){

  const adminIdToken = req.body.idToken;
  const usersData = req.body.usersData;
  const usersSingleData = req.body.usersSingleData;
  let errorsData = null;

  if(is.not.existy(adminIdToken)) {throw new Error('missing data idToken'); response.status(500);};
  if(is.not.existy(usersData)) {throw new Error('Sorry request failed'); response.status(500);};

  return adminInstance.auth().verifyIdToken(adminIdToken)
  .then(function(decodedToken) {
    var uid = decodedToken.uid;
    console.log(' Decoded token '+uid);
    return adminInstance.auth().importUsers(usersData);

  })
  .then(function(results){
    console.log("Data inserted ", usersData);
    // errorsData = results;
    // let updates = {};
    // for(var i=0;i<usersSingleData.length;i++)
    // {
    // 	updates['employees/'+usersSingleData[i].uid] = usersSingleData[i];
    //   updates[usersSingleData[i].usertype+'/'+usersSingleData[i].uid] = { time : (new Date).getTime()}
    // }
    // // updates["/Jobs/" + event.params.jobId] = { status: newStatus,  };
    // return adminInstance.database().ref().update(updates);

    // FS DB
    var batch = adminInstance.firestore().batch();
    for(var i=0;i<usersSingleData.length;i++)
    {
      var nycRef = adminInstance.firestore().collection('employees').doc(''+usersSingleData[i].uid);
      batch.set(nycRef, usersSingleData[i]);
      var nycRef2 = adminInstance.firestore().collection(''+usersSingleData[i].usertype).doc(''+usersSingleData[i].uid);
      batch.set(nycRef2, { time : (new Date).getTime()});
    }
    return batch.commit();

  })
  .then(function(results){
    res.status(200).send({ "response" : results, "error" : false });
  })
  .catch(function(error) {
    console.log('Error ', error);
    res.status(500);
  });
}



function disableUser(req,res, adminInstance){

  const adminIdToken = req.body.idToken;
  const userId = req.body.userId;
  let status = "enabled";
  if(req.body.status)
  if(req.body.status == "disabled")
  {
    status = "disabled";
  }

  // console.log(" Users data ", req.body);

  if(is.not.existy(adminIdToken)) {throw new Error('missing data idToken'); response.status(500);};
  if(is.not.existy(userId)) {throw new Error('Sorry request failed'); response.status(500);};

  return adminInstance.auth().verifyIdToken(adminIdToken)
  .then(function(decodedToken) {
    var uid = decodedToken.uid;
    console.log('  Decoded token '+uid);
    return adminInstance.auth().updateUser(userId, {
      disabled: status == "disabled" ? true : false
    });
  })
  .then(function(results){

    //RT DB
    // let updates = {};
    // updates['employees/'+userId+'/status'] = status;
    // // updates["/Jobs/" + event.params.jobId] = { status: newStatus,  };
    // return adminInstance.database().ref().update(updates);

    //FS DB
    var cityRef = adminInstance.firestore().collection('employees').doc(''+userId);
    return cityRef.update({ status: status });

  })
  .then(function(results){
    console.log("User "+(status == "disabled" ? " Disabled": "Enabled"), userId);
      res.status(200).send({ "response" : "User "+(status == "disabled"  ? " Disabled": "Enabled"), "error" : false });
  })
  .catch(function(error) {
    console.log('Error '+(status == "disabled"  ? " Disabled": "Enabled")+' User '+userId, error);
    res.status(500);
  });
}

module.exports = {
    importUsers : importUsers,
    disableUser : disableUser,
    addUser     : addUser
};
