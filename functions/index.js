const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const cors = require('cors')({origin: true});


var studentAPI = require('./functions_students');
var employeesAPI = require('./functions_employees');
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// On sign up.
exports.processSignUp = functions.auth.user().onCreate(event => {
  const user = event.data;
  // Check if user meets role criteria.
  if (user.email &&
      user.email.indexOf('admin@gmail.com') != -1) {
    const customClaims = {
      usertype : 'admin'
    };
    // Set custom user claims on this newly created user.
    return admin.auth().setCustomUserClaims(user.uid, customClaims)
      .then(() => {
        // Update real-time database to notify client to force refresh.
        const metadataRef = admin.database().ref("metadata/" + user.uid);
        // Set the refresh time to the current UTC timestamp.
        // This will be captured on the client to force a token refresh.
        console.log('Setting Admin Claim successful ');

        return metadataRef.set({refreshTime: new Date().getTime()});
      })
      .catch(error => {
        console.log('Error ');
      });
  }
});

exports.setCustomClaims = functions.https.onRequest((req, res) => {
  // Get the ID token passed.
  const adminIdToken = req.body.idToken;
  const userEmail = req.body.userEmail;
  const userType = req.body.userType;

  if(!!userEmail == true && !!userType == true && !!adminIdToken == ture){}
  else{ console.log('Error in Data'); res.end(JSON.stringify({status: 'ineligible', error : true}));}
  // Verify the ID token and decode its payload.
  admin.auth().verifyIdToken(adminIdToken)
  .then((verified)=>{
    console.log('User 1', userEmail, userType);
    return admin.auth().getUserByEmail(userEmail)
  })
  .then((user) => {
    // Confirm user is verified.
    if (user.emailVerified) {
      // Add custom claims for additional privileges.
      // This will be picked up by the user on token refresh or next sign in on new device.
      return admin.auth().setCustomUserClaims(user.uid, {
        usertype : userType
      });
    }
  }).then(function() {
    // Tell client to refresh token on user.
    res.end(JSON.stringify({
      status: 'success',
      error : false
    }));
  }).catch(function(){
    res.end(JSON.stringify({status: 'ineligible', error : true}));
  });

});



exports.getCustomClaims = functions.https.onRequest((req, res) => {
  // Get the ID token passed.
  const userId = req.body.userId;

  if(!!userId == true){}
  else{ console.log('Error in Data'); res.end(JSON.stringify({status: 'ineligible', error : true}));}
  // Verify the ID token and decode its payload.
  admin.auth().getUser(userId)
  .then(function(userRecord) {
    // Tell client to refresh token on user.
    console.log("fetched user record", userRecord);
    res.end(JSON.stringify({
      status: userRecord.customClaims,
      error : false
    }));
  }).catch(function(){
    res.end(JSON.stringify({status: 'ineligible', error : true}));
  });

});


/*
  STUDENT FUNCTIONS
  function - addUser
  function - adminUsersImport
  function - disableUser
*/
exports.addUser = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    return studentAPI.addUser(req, res, admin);
  });
});

exports.adminUsersImport = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    return studentAPI.importUsers(req, res, admin);
  });
});

exports.disableUser = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    return studentAPI.disableUser(req, res, admin);
  });
});


/*
  Employee FUNCTIONS
  function - addUser
  function - adminUsersImport
  function - disableUser
*/
exports.addEmployee = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    return employeesAPI.addUser(req, res, admin);
  });
});

exports.adminEmployeesImport = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    return employeesAPI.importUsers(req, res, admin);
  });
});

exports.disableEmployee = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    return employeesAPI.disableUser(req, res, admin);
  });
});
