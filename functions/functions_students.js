// const admin = require('firebase-admin');

var is = require('is_js');
// var db = require('./db');
// var app = db;

function addUser(req,res, adminInstance){

  const adminIdToken = req.body.idToken;
  const userData = req.body.userData;
  let userId = null;

  if(is.not.existy(adminIdToken)) {throw new Error('missing data idToken'); response.status(500);};
  if(is.not.existy(userData)) {throw new Error('Sorry request failed'); response.status(500);};

  return adminInstance.auth().verifyIdToken(adminIdToken)
  .then(function(decodedToken) {
    var uid = decodedToken.uid;
    console.log(' Decoded token '+uid);
    return adminInstance.auth().createUser({
        uid : userData.uid,
        email: userData.email,
        emailVerified: false,
        phoneNumber: userData.contact,
        password: "secretPassword",
        displayName: userData.firstname+' '+userData.lastname,
        disabled: false,
        customClaims: { usertype : "student"}
      });

  })
  .then(function(results){
    console.log("User Created ", userData);
    let updates = {};
    userData.status = "enabled";
    userData.contact = userData.phoneNumber;
    delete userData.phoneNumber;

    //RT DB
    //updates['students/'+userData.uid] = userData;
    //return adminInstance.database().ref().update(updates);

    //FS DB
    userId = userData.uid;

    return adminInstance.firestore().collection('students').doc(''+userData.uid).set(userData);
  })
  .then(function(response){
    return adminInstance.auth().setCustomUserClaims(userId, {
      usertype : 'student'
    })
  })
  .then(function(results){
    res.status(200).send({ "response" : results, "error" : false });
  })
  .catch(function(error) {
    console.log('Error ', error);
    res.status(500);
  });
}



function importUsers(req,res, adminInstance){

  const adminIdToken = req.body.idToken;
  const usersData = req.body.usersData;
  const usersSingleData = req.body.usersSingleData;
  let errorsData = null;

  if(is.not.existy(adminIdToken)) {throw new Error('missing data idToken'); response.status(500);};
  if(is.not.existy(usersData)) {throw new Error('Sorry request failed'); response.status(500);};

  return adminInstance.auth().verifyIdToken(adminIdToken)
  .then(function(decodedToken) {
    var uid = decodedToken.uid;
    console.log(' Decoded token '+uid);

    // return adminInstance.auth().importUsers([{
    //   uid: 'some-uid',
    //   displayName: 'John Doe',
    //   email: 'johndoe@gmail.com',
    //   customClaims: {student: true},
    // }]);

    return adminInstance.auth().importUsers(usersData);

  })
  .then(function(results){
    console.log("Data inserted ", usersData);
    errorsData = results;


    //RT DB
    // let updates = {};
    // for(var i=0;i<usersSingleData.length;i++)
    // {
    // 	updates['students/'+usersSingleData[i].uid] = usersSingleData[i];
    // }
    // return adminInstance.database().ref().update(updates);

    //FS DB
    // Get a new write batch
    var batch = adminInstance.firestore().batch();
    for(var i=0;i<usersSingleData.length;i++)
    {
      var nycRef = adminInstance.firestore().collection('students').doc(''+usersSingleData[i].uid);
      batch.set(nycRef, usersSingleData[i]);
    }
    return batch.commit();

  })
  .then(function(results){
    res.status(200).send({ "response" : results, "error" : false });
  })
  .catch(function(error) {
    console.log('Error ', error);
    res.status(500);
  });
}



function disableUser(req,res, adminInstance){

  const adminIdToken = req.body.idToken;
  const userId = req.body.userId;
  let status = "enabled";
  if(req.body.status)
  if(req.body.status == "disabled")
  {
    status = "disabled";
  }

  // console.log(" Users data ", req.body);

  if(is.not.existy(adminIdToken)) {throw new Error('missing data idToken'); response.status(500);};
  if(is.not.existy(userId)) {throw new Error('Sorry request failed'); response.status(500);};

  return adminInstance.auth().verifyIdToken(adminIdToken)
  .then(function(decodedToken) {
    var uid = decodedToken.uid;
    console.log('  Decoded token '+uid);
    return adminInstance.auth().updateUser(userId, {
      disabled: status == "disabled" ? true : false
    });
  })
  .then(function(results){

    //RT DB
    // let updates = {};
    // updates['students/'+userId+'/status'] = status;
    // return adminInstance.database().ref().update(updates);

    //FS DB
    var cityRef = adminInstance.firestore().collection('students').doc(''+userId);
    return cityRef.update({ status: status });

  })
  .then(function(results){
    console.log("User "+(status == "disabled" ? " Disabled": "Enabled"), userId);
      res.status(200).send({ "response" : "User "+(status == "disabled"  ? " Disabled": "Enabled"), "error" : false });
  })
  .catch(function(error) {
    console.log('Error '+(status == "disabled"  ? " Disabled": "Enabled")+' User '+userId, error);
    res.status(500);
  });
}

module.exports = {
    importUsers : importUsers,
    disableUser : disableUser,
    addUser     : addUser
};
